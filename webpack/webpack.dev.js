const merge = require("webpack-merge");
const common = require("./webpack.common.js");
const rootPath = require("./rootPath");

module.exports = merge(common, {
  mode: "development",

  devtool: "#eval-source-map",

  devServer: {
    contentBase: rootPath("public"),
    compress: true,
    port: 8000,

    historyApiFallback: true
  }
});
