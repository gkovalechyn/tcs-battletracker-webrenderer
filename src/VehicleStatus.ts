export enum VehicleStatus {
  DEFAULT = 0,
  IN_VEHILE = 1,
  UNCONSCIOUS = 2,
  DEAD = 3
}
