//https://github.com/Microsoft/TypeScript-Vue-Starter
var path = require("path");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const CleanObsoleteChunks = require("webpack-clean-obsolete-chunks");
const rootPath = require("./rootPath");


module.exports = {
  entry: {
    app: [rootPath("src/app.ts")]
  },

  output: {
    path: rootPath("public/"),
    filename: "bin/js/[name].js",
    chunkFilename: "bin/js/[id].[name].js"
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/]
        }
      },

      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]?[hash]"
        }
      }
    ]
  },

  plugins: [
    new CleanObsoleteChunks()
  ],

  resolve: {
    extensions: [".ts", ".js", ".json"],
    plugins: [new TsconfigPathsPlugin()]
  },

  performance: {
    hints: false
  }
};
