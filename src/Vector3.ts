import { BufferProxy } from "./BufferProxy";

export class Vector3 {
  public get x(): number {
    return this._x;
  }

  public set x(v: number) {
    this._x = v;
  }

  public get y(): number {
    return this._y;
  }

  public set y(v: number) {
    this._y = v;
  }

  public get z(): number {
    return this._z;
  }

  public set z(v: number) {
    this._z = v;
  }
  public static lerp(from: Vector3, to: Vector3, dt: number): Vector3 {
    return to
      .subtracted(from)
      .multiply(dt)
      .add(from);
  }

  /**
   * Returns the dot product between the 2 vectors.
   * @param v1 Vector 1
   * @param v2 Vector 1
   */
  public static dot(v1: Vector3, v2: Vector3): number {
    return v1._x * v2._x + v1._y * v2._y + v1._z * v2._z;
  }

  /**
   * Returns the angle between 2 vectors.
   * @param v1 First vector
   * @param v2 Second vector
   */
  public static angleBetween(v1: Vector3, v2: Vector3): number {
    return Math.acos(Vector3.dot(v1, v2) / (v1.magnitude() * v2.magnitude()));
  }

  /**
   * Returns a direction vector on the X-Y plane based on the angle given.
   * @param angle The angle in radians.
   */
  public static fromAngle(angle: number): Vector3 {
    return new Vector3(Math.sin(angle), Math.cos(angle));
  }

  /**
   * Returns the UP vector in 3d space.
   */
  public static Up(): Vector3 {
    return new Vector3(0, 1, 0);
  }

  /**
   * Returns a vector pointing to the right in 3D space.
   */
  public static Right(): Vector3 {
    return new Vector3(1, 0, 0);
  }

  public static fromBuffer(buffer: BufferProxy) {
    return new Vector3(
      buffer.nextFloat32(),
      buffer.nextFloat32(),
      buffer.nextFloat32()
    );
  }

  private _x = 0;
  private _y = 0;
  private _z = 0;

  public constructor(x = 0, y = 0, z = 0) {
    this._x = x;
    this._y = y;
    this._z = z;
  }

  public squaredMagnigude() {
    return this._x * this._x + this._y * this._y + this._z * this._z;
  }

  public magnitude(): number {
    return Math.sqrt(this._x * this._x + this._y * this._y + this._z * this._z);
  }

  /**
   * Normalizes this vector so that the magnitude is equal to 1.
   * Returns THIS vector.
   */
  public normalize(): Vector3 {
    const magnitude = this.magnitude();

    this._x /= magnitude;
    this._y /= magnitude;
    this._z /= magnitude;

    return this;
  }

  /**
   * Adds another vector to this vector.
   * returns THIS vector.
   * @param other The other vector.
   */
  public add(other: Vector3) {
    this._x += other._x;
    this._y += other._y;
    this._z += other._z;

    return this;
  }

  /**
   * Returns a NEW vector with the result of this vector added to the other one.
   * @param other The other vector
   */
  public added(other: Vector3) {
    return new Vector3(
      this._x + other._x,
      this._y + other._y,
      this._z + other._z
    );
  }

  /**
   * Returns a NEW vector that is the result of this vector subtracted the other vector.
   * @param other The other vector
   */
  public subtracted(other: Vector3): Vector3 {
    return new Vector3(
      this._x - other._x,
      this._y - other._y,
      this._z - other._z
    );
  }

  /**
   * Multiplies this vector by the given value.
   * Returns THIS vector.
   * @param value The value
   */
  public multiply(value: number): Vector3 {
    this._x *= value;
    this._y *= value;
    this._z *= value;

    return this;
  }

  /**
   * Divides this vector by the given value.
   * Returns THIS vector.
   * @param value The value
   */
  public divide(value: number): Vector3 {
    this._x /= value;
    this._y /= value;
    this._z /= value;

    return this;
  }

  public clone() {
    return new Vector3(this._x, this._y, this._z);
  }
  /**
   * Returns the angle of this vector to the X axis.
   */
  public getAngleXAxis(): number {
    return Math.atan2(this._x, this._y);
  }
}
