import { Event } from "./Events/Event";
import { FiredEvent } from "./Events/FiredEvent";
import { HitEvent } from "./Events/HitEvent";
import { KillEvent } from "./Events/KillEvent";
import { Snapshot } from "./Snapshot";
import { Vector3 } from "./Vector3";
import { BufferProxy } from "./BufferProxy";
/**
 * A timeline consists of eveything that happened to the unit during the mission.
 */
export class Timeline {
  public static fromBuffer(buffer: BufferProxy): Timeline {
    const timeline = new Timeline();
    // Snapshots
    const snapshotCount = buffer.nextInt32();
    timeline.snapshots = new Array<Snapshot>(snapshotCount);
    for (let i = 0; i < snapshotCount; i++) {
      timeline.snapshots[i] = Snapshot.fromBuffer(buffer);
    }

    // Events
    const eventCount = buffer.nextInt32();
    timeline.events = new Array<Event>(eventCount);
    for (let i = 0; i < eventCount; i++) {
      const eventType = buffer.nextInt32();
      const bodyLength = buffer.nextInt32();
      switch (eventType) {
        case FiredEvent.TYPE_ID:
          timeline.events[i] = new FiredEvent(
            buffer.nextFloat32(),
            Vector3.fromBuffer(buffer),
            Vector3.fromBuffer(buffer),
            buffer.nextFloat32()
          );
          break;
        case HitEvent.TYPE_ID:
          {
            const timestamp = buffer.nextFloat32();
            const myPosition = Vector3.fromBuffer(buffer);
            const instigatorPosition = Vector3.fromBuffer(buffer);
            const instigatorID = buffer.nextInt32();

            timeline.events[i] = new HitEvent(
              timestamp,
              instigatorID,
              instigatorPosition,
              myPosition
            );
          }
          break;
        case KillEvent.TYPE_ID:
          {
            const timestamp = buffer.nextFloat32();
            const myPosition = Vector3.fromBuffer(buffer);
            const victimPosition = Vector3.fromBuffer(buffer);
            const victimID = buffer.nextInt32();
            timeline.events[i] = new KillEvent(
              timestamp,
              victimID,
              victimPosition,
              myPosition
            );
          }
          break;
        default:
          buffer.skip(bodyLength);
      }
    }
    return timeline;
  }

  private snapshots: Snapshot[];
  private events: Event[];

  public constructor() {
    this.snapshots = Array<Snapshot>();
    this.events = Array<Event>();
  }

  /**
   * Returns all the events that happened to the unit that owns this timeline.
   */
  public get Events(): Event[] {
    return this.events;
  }

  /**
   * Returns all the snapshots taken for the unit that owns this timeline.
   */
  public get Snapshots(): Snapshot[] {
    return this.snapshots;
  }
}
