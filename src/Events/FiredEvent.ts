import { Event } from "./Event";
import { Vector3 } from "../Vector3";
import { Map } from "leaflet";
import { RasterCoords } from "../RasterCoords";

export class FiredEvent extends Event {
  public static readonly TYPE = "Fired";
  public static readonly TYPE_ID = 3;

  private _firedPostion: Vector3;
  private _projectileHitPosition: Vector3;
  private _projectileHitTimestamp: number;

  constructor(
    timestamp: number,
    firedPosition: Vector3,
    projectileHitPosition: Vector3,
    projectileHitTimestamp: number
  ) {
    super(timestamp);
    this._firedPostion = firedPosition;
    this._projectileHitPosition = projectileHitPosition;
    this._projectileHitTimestamp = projectileHitTimestamp;
  }

  public onEnterScene(
    map: Map,
    time: number,
    coordenatesTransformer: RasterCoords
  ): void {
    // Nothing
  }
  public onUpdate(
    map: Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void {
    // Nothing
  }
  public onLeaveScene(
    map: Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void {
    // Nothing
  }

  public getTypeID(): number {
    return 3;
  }

  public isActiveAt(time: number): boolean {
    return time > this.Timestamp && time < this.ProjectileHitTimestamp;
  }

  public getType(): string {
    return FiredEvent.TYPE;
  }

  public get FiredPosition(): Vector3 {
    return this._firedPostion;
  }

  public get ProjectileHitPosition(): Vector3 {
    return this._projectileHitPosition;
  }

  public get ProjectileHitTimestamp(): number {
    return this._projectileHitTimestamp;
  }
}
