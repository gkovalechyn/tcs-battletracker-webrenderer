import { BattleTrackerRenderer } from "./BattleTrackerRenderer";
import { Recording } from "./Recording";
import { IconCache } from "./IconCache";
import { VehicleType } from "./VehicleType";
import { VehicleStatus } from "./VehicleStatus";
import { Side } from "./Side";
/**
 * Entrypoint for the local dev-server used for testing
 */
class App {
  private renderer!: BattleTrackerRenderer;

  public constructor() {
    // const pageCanvas = document.getElementById("canvas") as HTMLCanvasElement;
    this.renderer = new BattleTrackerRenderer(document.getElementById("map")!);
    this.renderer.setTileURL("/tiles/{z}/{x}/{y}.png");
    IconCache.createIconsFor(
      "/icons/Man-Opfor.png",
      VehicleType.MAN,
      VehicleStatus.DEFAULT,
      Side.EAST
    );
    IconCache.createIconsFor(
      "/icons/Man-Blufor.png",
      VehicleType.MAN,
      VehicleStatus.DEFAULT,
      Side.WEST
    );

    IconCache.createIconsFor(
      "/icons/Man-Dead.png",
      VehicleType.MAN,
      VehicleStatus.DEAD,
      Side.WEST
    );

    IconCache.createIconsFor(
      "/icons/Man-Dead.png",
      VehicleType.MAN,
      VehicleStatus.DEAD,
      Side.EAST
    );
  }

  public onStartClicked() {
    this.renderer.play();
  }

  public onPauseClicked() {
    this.renderer.pause();
  }

  public onStopClicked() {
    this.renderer.stop();
  }

  public onFileChanged() {
    const fileInput = document.getElementById("fileInput") as HTMLInputElement;
    if (fileInput.files) {
      const file = fileInput.files[0];
      const reader = new FileReader();

      reader.readAsArrayBuffer(file);
      reader.onload = () => {
        this.renderer.setRecording(
          Recording.fromArrayBuffer(reader.result as ArrayBuffer)
        );
      };
    }
  }
}

const app = new App();

(window as any).app = app;
