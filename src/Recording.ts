import { MissionData } from "./MissionData";
import { Vehicle } from "./Vehicle";
import { BufferProxy } from "./BufferProxy";

/**
 * Represents a recording of a mission.
 */
export class Recording {
  /**
   * Returns the {@link MissionData} for the mission
   */
  public get MissionData(): MissionData {
    return this._missionData;
  }

  /**
   * Returns a map with all the units that were in the mission
   * during the duration of the recording.
   */
  public get Units(): Map<string, Vehicle> {
    return this._units;
  }

  /**
   * Returns the version of the format of this recording.
   * The current, and only, version is 1.0.0
   */
  public get Version(): string {
    return this._version;
  }

  // BT Characters
  public static MAGIC = [0x42, 0x54];

  /**
   * Creates a recording from a binary buffer, the data must be in the format
   * specified by the "Recording.ksy" file in the extension.
   *
   * @param arrayBuffer The array buffer that is a recording.
   */
  public static fromArrayBuffer(arrayBuffer: ArrayBuffer) {
    const buffer = new BufferProxy(arrayBuffer);
    const asciiB = buffer.nextInt8();
    const asciiT = buffer.nextInt8();

    if (asciiB !== this.MAGIC[0] || asciiT !== this.MAGIC[1]) {
      throw new Error("The buffer is not a BattleTracker recording");
    }
    const version = `${buffer.nextInt32()}.${buffer.nextInt32()}.${buffer.nextInt32()}`;
    const missionData = MissionData.fromBuffer(buffer);
    const recording = new Recording(missionData);

    recording._version = version;

    const vehicleCount = buffer.nextInt32();
    for (let i = 0; i < vehicleCount; i++) {
      const vehicle = Vehicle.fromBuffer(buffer);
      recording._units.set(vehicle.ID.toString(), vehicle);
    }

    return recording;
  }

  private _missionData: MissionData;
  private _units: Map<string, Vehicle> = new Map<string, Vehicle>();
  private _version: string = "";
  private constructor(missionData: MissionData) {
    this._missionData = missionData;
  }
}
