import * as Leaflet from "leaflet";

/**
 * leaflet plugin for plain image map projection
 * @copyright 2016- commenthol
 * Adpapted to typescript by gkovalechyn (gkovalechyn.net | github.com/gkovalechyn | gkovalechyn@gmail.com)
 * @license MIT
 */
/* globals define */

export class RasterCoords {
  private map: Leaflet.Map;
  private width: number;
  private height: number;
  private tileSize: number;
  private zoom: number;
  /**
   * @param {L.map} map - the map used
   * @param {Array} imgsize - [ width, height ] image dimensions
   * @param {Number} [tilesize] - tilesize in pixels. Default=256
   */
  public constructor(
    map: Leaflet.Map,
    worldSize: [number, number],
    tileSize = 256
  ) {
    this.map = map;
    this.width = worldSize[0];
    this.height = worldSize[1];
    this.tileSize = tileSize;
    this.zoom = this.zoomLevel();
  }

  /**
   * calculate accurate zoom level for the given image size
   */
  public zoomLevel() {
    return Math.ceil(
      Math.log(Math.max(this.width, this.height) / this.tileSize) / Math.log(2)
    );
  }

  /**
   * Updates the internal width and height to be used in the calculation of the projections.
   * @param width The new width
   * @param height the new height
   */
  public setWorldSize(width: number, height: number) {
    this.width = width;
    this.height = height;
    this.zoom = this.zoomLevel();
  }

  public getHeight() {
    return this.height;
  }

  public getWidth() {
    return this.width;
  }
  /**
   * unproject `coords` to the raster coordinates used by the raster image projection
   * @param {Leaflet.PointExpression} coords - [ x, y ]
   * @return {L.LatLng} - internal coordinates
   */
  public toLeaflet(coords: Leaflet.PointExpression) {
    return this.map.unproject(coords, this.zoom);
  }

  /**
   * project `coords` back to image coordinates
   * @param {Leaflet.LatLngExpression} coords - [ x, y ]
   * @return {L.LatLng} - image coordinates
   */
  public toWorld(coords: Leaflet.LatLngExpression) {
    return this.map.project(coords, this.zoom);
  }

  /**
   * sets the max bounds on map
   */
  public setMaxBounds() {
    const southWest = this.toLeaflet([0, this.height]);
    const northEast = this.toLeaflet([this.width, 0]);
    this.map.setMaxBounds(new Leaflet.LatLngBounds(southWest, northEast));
  }
}
