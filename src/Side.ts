export enum Side {
  WEST = 0,
  EAST = 1,
  INDEPENDENT = 2,
  CIVILIAN = 3,
  UNKNOWN = 4
}
