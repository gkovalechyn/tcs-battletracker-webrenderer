import { Transform } from "./Transform";
import { VehicleStatus } from "./VehicleStatus";
import { BufferProxy } from "./BufferProxy";

export class Snapshot {

  public static fromBuffer(buffer: BufferProxy) {
    return new Snapshot(
      buffer.nextFloat32(),
      buffer.nextInt32() as VehicleStatus,
      Transform.fromBuffer(buffer)
    );
  }

  private _timestamp: number;
  private _transform: Transform;
  private _status: VehicleStatus;

  public constructor(
    timestamp = 0,
    status = VehicleStatus.DEFAULT,
    transform = new Transform()
  ) {
    this._timestamp = timestamp;
    this._status = status;
    this._transform = transform;
  }

  public get Timestamp(): number {
    return this._timestamp;
  }

  public get Transform(): Transform {
    return this._transform;
  }

  public get Status(): VehicleStatus {
    return this._status;
  }

  public clone() {
    return new Snapshot(this._timestamp, this._status, this._transform.clone());
  }
}
