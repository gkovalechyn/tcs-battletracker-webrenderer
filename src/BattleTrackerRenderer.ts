import { Recording } from "./Recording";
import { Event } from "./Events/Event";
import * as Leaflet from "leaflet";
import { Vehicle } from "./Vehicle";
import { RasterCoords } from "./RasterCoords";

/**
 * The renderer is used to display and control the playback of a {@link Recording}.
 */
export class BattleTrackerRenderer {
  private map: Leaflet.Map;
  private tileLayer: Leaflet.TileLayer;
  // Yes we need this, we can't grab the old url from the tile layer
  private tileLayerURL: string = "";
  private rasterCoords: RasterCoords;

  private _playbackSpeed = 1;
  private _debugInformationEnabled = false;

  private recording: Recording | null = null;
  private playing = false;

  private currentRecordingTime = 0;
  private lastLoopTime = 0;

  private activeVehicles = new Set<Vehicle>();
  private inactiveVehicles = new Set<Vehicle>();

  private activeEvents = new Set<Event>();
  /**
   * Creates a new instance of the renderer.
   *
   * @param mapElement Where should the map be placed.
   */
  public constructor(mapElement: HTMLElement) {
    this.map = Leaflet.map(mapElement);
    this.map.setView([0, 0], 0);

    // Empty layer just because, but also prevent it from loading anything
    this.tileLayer = Leaflet.tileLayer("", {
      noWrap: true
    }).addTo(this.map);

    // Set it up with a default world size that should work for most maps
    this.rasterCoords = new RasterCoords(this.map, [16384, 16384]);

    window.requestAnimationFrame(() => this.loop());
  }

  /**
   * Starts the playback
   */
  public play() {
    if (this.recording && !this.playing) {
      this.playing = true;
      this.lastLoopTime = Date.now();
    }
  }

  /**
   * Returns the leaflet map that is used to draw the map.
   */
  public getMap(): Leaflet.Map {
    return this.map;
  }

  /**
   * Stops the recording from playing and returns the current recording time to the start of the mission.
   */
  public stop() {
    if (this.recording) {
      this.currentRecordingTime = this.recording.MissionData.StartTime;
    }
    this.playing = false;
  }

  /**
   * Pauses the recording playback.
   */
  public pause() {
    this.playing = false;
  }

  /**
   * Changes the recording that will be played back.
   * @param recording The recording that will be played.
   */
  public setRecording(recording: Recording) {
    if (this.playing) {
      throw new Error(
        "Playback must be stopped before changing recording files"
      );
    }
    this.recording = recording;

    for (const vehicle of this.activeVehicles) {
      vehicle.onLeaveScene(
        this.map,
        this.rasterCoords,
        this.currentRecordingTime
      );
    }

    for (const event of this.activeEvents) {
      event.onLeaveScene(
        this.map,
        this.rasterCoords,
        this.currentRecordingTime
      );
    }

    this.activeVehicles.clear();
    this.inactiveVehicles.clear();
    this.activeEvents.clear();

    if (recording) {
      this.currentRecordingTime = recording.MissionData.StartTime;
      this.rasterCoords.setWorldSize(
        recording.MissionData.WorldSize.x,
        recording.MissionData.WorldSize.y
      );

      // We can't update the layer bounds at runtime, so we need to create a new one
      // There might be a way to keep the cache of the layer but I haven't looked into it
      this.recreateTileLayer();

      for (const entry of recording.Units) {
        this.inactiveVehicles.add(entry[1]);
      }
    }
  }

  public setTileURL(url: string) {
    this.tileLayerURL = url;
    this.recreateTileLayer();
  }

  /**
   * Returns the current playback speed.
   */
  public get PlaybackSpeed(): number {
    return this._playbackSpeed;
  }

  /**
   * Sets the playback speed where: 1 = real time, 2 = twice as fast, 0.5 = half-speed, etc.
   */
  public set PlaybackSpeed(v: number) {
    this._playbackSpeed = v;
  }

  /**
   * Sets the recording time to the given time.
   * The value must be in game seconds.
   */
  public set CurrentRecordingTime(v: number) {
    this.currentRecordingTime = v;
  }

  /**
   * Returns the current recording time in in-game seconds.
   */
  public get CurrentRecordingTime(): number {
    return this.currentRecordingTime;
  }

  /**
   * Returns wheter the renderer is playing a recording or not.
   */
  public isPlaying(): boolean {
    return this.playing;
  }

  /**
   * Sets whether to show debug information on the screen or not.
   */
  public set DebugInformationEnabled(v: boolean) {
    this._debugInformationEnabled = v;
  }

  /**
   * Returns whether showing debug information on the canvas is enabled or not.
   */
  public get DebugInformationEnabled(): boolean {
    return this._debugInformationEnabled;
  }

  private recreateTileLayer() {
    this.tileLayer.remove();

    if (this.recording) {
      const bounds = new Leaflet.LatLngBounds(
        this.rasterCoords.toLeaflet([0, 0]),
        this.rasterCoords.toLeaflet([
          this.recording.MissionData.WorldSize.x,
          this.recording.MissionData.WorldSize.y
        ])
      );

      this.tileLayer = Leaflet.tileLayer(this.tileLayerURL, {
        bounds,
        noWrap: true
      }).addTo(this.map);
    } else {
      this.tileLayer = Leaflet.tileLayer(this.tileLayerURL, {
        noWrap: true
      }).addTo(this.map);
    }
  }

  private loop() {
    if (this.playing) {
      const dt = Date.now() - this.lastLoopTime;
      this.lastLoopTime = Date.now();
      this.CurrentRecordingTime += (dt * this._playbackSpeed) / 1000; // The recording time is in seconds

      // For now I've just put everything into this if
      // But ideally you wouldn't even call the loop if the playback is stopped
      if (this.recording) {
        if (this.currentRecordingTime > this.recording.MissionData.EndTime) {
          this.stop();
        }

        // Update all active units and then check the inactive ones and update them
        for (const vehicle of this.activeVehicles) {
          if (vehicle.existsInGameAt(this.currentRecordingTime)) {
            vehicle.onUpdate(
              this.map,
              this.rasterCoords,
              this.currentRecordingTime
            );

            for (const event of vehicle.getActiveEventsAt(
              this.currentRecordingTime
            )) {
              if (!this.activeEvents.has(event)) {
                event.onEnterScene(
                  this.map,
                  this.currentRecordingTime,
                  this.rasterCoords
                );

                this.activeEvents.add(event);
              }
            }
          } else {
            vehicle.onLeaveScene(
              this.map,
              this.rasterCoords,
              this.currentRecordingTime
            );

            this.activeVehicles.delete(vehicle);
            this.inactiveVehicles.add(vehicle);
          }
        }

        for (const vehicle of this.inactiveVehicles) {
          if (vehicle.existsInGameAt(this.currentRecordingTime)) {
            this.inactiveVehicles.delete(vehicle);
            this.activeVehicles.add(vehicle);

            vehicle.onEnterScene(
              this.map,
              this.currentRecordingTime,
              this.rasterCoords
            );
          }
        }

        for (const event of this.activeEvents) {
          if (!event.isActiveAt(this.currentRecordingTime)) {
            event.onLeaveScene(
              this.map,
              this.rasterCoords,
              this.currentRecordingTime
            );
            this.activeEvents.delete(event);
          }
        }
      }
    }

    window.requestAnimationFrame(() => this.loop());
  }
}
