import { Event } from "./Event";
import { Vector3 } from "../Vector3";
import { RasterCoords } from "../RasterCoords";
import * as Leaflet from "leaflet";
import { IconCache } from "../IconCache";

export class HitEvent extends Event {
  public static readonly TYPE = "Hit";
  public static readonly TYPE_ID = 4;

  private _instigatorID: number;
  private _instigatorPosition: Vector3;
  private _myPosition: Vector3;

  private _polygon: Leaflet.Polygon | null = null;

  constructor(
    timestamp: number,
    instigatorID: number,
    instigatorPosition: Vector3,
    myPosition: Vector3
  ) {
    super(timestamp);
    this._instigatorID = instigatorID;
    this._instigatorPosition = instigatorPosition;
    this._myPosition = myPosition;
  }

  public onEnterScene(
    map: Leaflet.Map,
    time: number,
    coordenatesTransformer: RasterCoords
  ): void {
    if (this._polygon) {
      this._polygon.remove();
    }

    const toVictim = this._myPosition.subtracted(this._instigatorPosition);
    // No idea why this is 4, it should be 2 but by 2 it's double the correct value.
    const iconSize = IconCache.getIconSizeAtZoomLevel(map.getZoom()) / 4;

    const rotationAngle = toVictim.getAngleXAxis();
    const leftPoint = this._instigatorPosition.added(
      new Vector3(
        -iconSize * Math.cos(rotationAngle),
        iconSize * Math.sin(rotationAngle)
      )
    );
    const rightPoint = this._instigatorPosition.added(
      new Vector3(
        iconSize * Math.cos(rotationAngle),
        -iconSize * Math.sin(rotationAngle)
      )
    );
    const targetPoint = this._myPosition;

    const mapPoint1 = coordenatesTransformer.toLeaflet([
      leftPoint.x,
      coordenatesTransformer.getHeight() - leftPoint.y
    ]);
    const mapPoint2 = coordenatesTransformer.toLeaflet([
      rightPoint.x,
      coordenatesTransformer.getHeight() - rightPoint.y
    ]);
    const mapPoint3 = coordenatesTransformer.toLeaflet([
      targetPoint.x,
      coordenatesTransformer.getHeight() - targetPoint.y
    ]);

    this._polygon = Leaflet.polygon([mapPoint1, mapPoint2, mapPoint3], {
      fillColor: "#000000",
      fillOpacity: 0.5,
      stroke: false
    });

    this._polygon.addTo(map);
  }
  public onUpdate(
    map: Leaflet.Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void {
    // Nothing
  }
  public onLeaveScene(
    map: Leaflet.Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void {
    if (this._polygon) {
      this._polygon.remove();
    }
  }

  public getType(): string {
    return HitEvent.TYPE;
  }

  public getTypeID(): number {
    return 4;
  }

  public isActiveAt(time: number): boolean {
    return time >= this.Timestamp && time < this.Timestamp + 3;
  }

  public get InstigatorID(): number {
    return this._instigatorID;
  }

  public get IinstigatorPosition(): Vector3 {
    return this._instigatorPosition;
  }

  public get MyPosition(): Vector3 {
    return this._myPosition;
  }
}
