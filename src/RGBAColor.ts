import { Side } from "./Side";

/**
 * Struct representing a color with 4 channels, red, green, blue and alpha.
 * Each channel can have a value ranging from 0.0 to 1.0
 */
export class RGBAColor {
  public static readonly RED = new RGBAColor(1.0, 0.0, 0.0);
  public static readonly GREEN = new RGBAColor(0.0, 1.0, 0.0);
  public static readonly BLUE = new RGBAColor(0.0, 0.0, 1.0);

  public static readonly WHITE = new RGBAColor(1.0, 1.0, 1.0);
  public static readonly BLACK = new RGBAColor(0.0, 0.0, 0.0);

  public static readonly WEST = new RGBAColor(0.0, 0.3, 0.6, 1.0);
  public static readonly EAST = new RGBAColor(0.5, 0.0, 0.0, 1.0);
  public static readonly INDEPENDENT = new RGBAColor(0.0, 0.5, 0.0, 1.0);
  public static readonly CIV = new RGBAColor(0.4, 0.0, 0.5, 1.0);
  public static readonly UNKNOWN = new RGBAColor(0.7, 0.6, 0.0, 1.0);

  public static getSideColor(side: Side): RGBAColor {
    switch (side) {
      case Side.WEST:
        return RGBAColor.WEST;
      case Side.EAST:
        return RGBAColor.EAST;
      case Side.INDEPENDENT:
        return RGBAColor.INDEPENDENT;
      case Side.CIVILIAN:
        return RGBAColor.CIV;
      case Side.UNKNOWN:
        return RGBAColor.UNKNOWN;
      default:
        throw new Error("Unkwown side: " + side);
    }
  }

  private _r: number = 0;
  private _g: number = 0;
  private _b: number = 0;
  private _a: number = 1.0;

  public constructor(r = 0.0, g = 0.0, b = 0.0, a = 1.0) {
    this._r = r;
    this._g = g;
    this._b = b;
    this._a = a;
  }

  /**
   * Returns a css color string in the following format:
   *  rgba(red, green, blue, alpha)
   */
  public getCSSString(): string {
    return (
      "rgba(" +
      Math.round(this._r * 255) +
      "," +
      Math.round(this._g * 255) +
      "," +
      Math.round(this._b * 255) +
      "," +
      Math.round(this._a) +
      ")"
    );
  }

  /**
   * Returns the value of the red channel clamped between 0.0 and 1.0
   */
  get r(): number {
    return this._r;
  }

  /**
   * Sets the value for the red channel of this color.
   * If the value is greater than 1 it is clamped to 1.
   * If the value is lower than 0 it is clamped to 0.
   */
  set r(val: number) {
    if (val > 1) {
      val = 1;
    } else if (val < 0) {
      val = 0;
    }

    this._r = val;
  }

  /**
   * Returns the value of the green channel clamped between 0.0 and 1.0
   */
  get g(): number {
    return this._g;
  }

  /**
   * Sets the value for the green channel of this color.
   * If the value is greater than 1 it is clamped to 1.
   * If the value is lower than 0 it is clamped to 0.
   */
  set g(val: number) {
    if (val > 1) {
      val = 1;
    } else if (val < 0) {
      val = 0;
    }

    this._g = val;
  }

  /**
   * Returns the value of the blue channel clamped between 0.0 and 1.0
   */
  get b(): number {
    return this._b;
  }

  /**
   * Sets the value for the blue channel of this color.
   * If the value is greater than 1 it is clamped to 1.
   * If the value is lower than 0 it is clamped to 0.
   */
  set b(val: number) {
    if (val > 1) {
      val = 1;
    } else if (val < 0) {
      val = 0;
    }

    this._b = val;
  }

  /**
   * Returns the value of the alpha channel clamped between 0.0 and 1.0
   */
  get a(): number {
    return this._a;
  }

  /**
   * Sets the value for the alpha channel of this color.
   * If the value is greater than 1 it is clamped to 1.
   * If the value is lower than 0 it is clamped to 0.
   */
  set a(val: number) {
    if (val > 1) {
      val = 1;
    } else if (val < 0) {
      val = 0;
    }

    this._a = val;
  }
}
