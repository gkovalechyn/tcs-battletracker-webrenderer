# Battletracker Web Renderer


## Usage
Install the package by using NPM
```batch
 npm install battletracker-web-renderer
```

The `BattleTrackerRenderer` class does all of the work of managing where the recording is and displaying all the units on the map. To get it to work what you need to do is:
* Give it a place to put the map, same as you would do with [leaflet](leafletjs.com).
* Give it a `Recording` to play.
* Tell it which tiles to use, the format is any format [leaflet](leafletjs.com) accepts.
* Cache the unit icons to be used.

Example:
```js
import { BattleTrackerRenderer, Recording } from "battletracker-web-renderer";
import { VehicleType } from "battletracker-web-renderer/lib/VehicleType";
import { VehicleStatus } from "battletracker-web-renderer/lib/VehicleStatus";
import { Side } from "battletracker-web-renderer/lib/Side";
import { IconCache } from "battletracker-web-renderer/lib/IconCache";

const renderer = new BattleTrackerRenderer(document.getElementById("map"));

// Load the file from somewhere like an input tag
const file;
let fileReader = new FileReader();

fileReader.onload = (ev) => {
  renderer.setRecording(Recording.fromArrayBuffer(ev.target.result));
};

// Recordings are binary files that must follow the format specified in
// "Recording.ksy" in the C++ extension repository.
// It is also possible to load them from JSON objects but that has been deprecated.
reader.readAsArrayBuffer(file);

// Tell it where to load the tiles from
renderer.setTileURL("/tiles/chernarus/{z}/{x}/{y}.png");

// By default it does not come with any icons, you need to supply them yourself
// if no icons are supplied it will use the default leaflet marker icon.
// You need to specify icons for each side, vehicle type and status, you can do
// so by using the `createIconsFor` function in the IconCache
IconCache.createIconsFor(
  "/icons/Man-Blufor.png", 
  VehicleType.MAN,
  VehicleStatus.DEFAULT,
  Side.WEST
);

// Due to a limitation in leaflet the icons need to be created for each zoom level
// so if you don't adjust their size, some icons may be too small, like the tank or car icons
// to adjust the size of the icons being created you can use the setIconSizes function
// or you can just get the array and modify it
const iconSizes = IconCache.getIconSizes();
for (let i = 0; i < iconSizes; i++){
  // The index is the leaflet zoom level
  iconSizes[i] = iconSizes[i] + 10;
}

// The tank icons created are now 10px bigger on every zoom level
// than the man icons
IconCache.createIconsFor(
  "/icons/Tank-Blufor.png", 
  VehicleType.TANK,
  VehicleStatus.DEFAULT,
  Side.WEST
);

// Then just start the playback
renderer.play();
```

The renderer provides some methods to aid in playing back the recording and setting where to start playing from.