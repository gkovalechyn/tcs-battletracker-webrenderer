import { Event } from "../Events/Event";
import { Vector3 } from "../Vector3";
import { RasterCoords } from "../RasterCoords";
import * as Leaflet from "leaflet";
import { IconCache } from "../IconCache";

export class KillEvent extends Event {
  public static readonly TYPE = "Kill";
  public static readonly TYPE_ID = 5;

  private _victimID: number;
  private _victimPosition: Vector3;
  private _myPosition: Vector3;

  private _polygon: Leaflet.Polygon | null = null;

  constructor(
    timestamp: number,
    victimID: number,
    victimPosition: Vector3,
    myPosition: Vector3
  ) {
    super(timestamp);
    this._victimID = victimID;
    this._victimPosition = victimPosition;
    this._myPosition = myPosition;
  }

  public onEnterScene(
    map: Leaflet.Map,
    time: number,
    coordenatesTransformer: RasterCoords
  ): void {
    if (this._polygon) {
      this._polygon.remove();
    }

    const toVictim = this._victimPosition.subtracted(this._myPosition);
    // No idea why this is 4, it should be 2 but by 2 it's double the correct value.
    const iconSize = IconCache.getIconSizeAtZoomLevel(map.getZoom()) / 4;

    const rotationAngle = toVictim.getAngleXAxis();
    const leftPoint = this._myPosition.added(
      new Vector3(
        -iconSize * Math.cos(rotationAngle),
        iconSize * Math.sin(rotationAngle)
      )
    );
    const rightPoint = this._myPosition.added(
      new Vector3(
        iconSize * Math.cos(rotationAngle),
        -iconSize * Math.sin(rotationAngle)
      )
    );
    const targetPoint = this._victimPosition;

    const mapPoint1 = coordenatesTransformer.toLeaflet([
      leftPoint.x,
      coordenatesTransformer.getHeight() - leftPoint.y
    ]);
    const mapPoint2 = coordenatesTransformer.toLeaflet([
      rightPoint.x,
      coordenatesTransformer.getHeight() - rightPoint.y
    ]);
    const mapPoint3 = coordenatesTransformer.toLeaflet([
      targetPoint.x,
      coordenatesTransformer.getHeight() - targetPoint.y
    ]);

    this._polygon = Leaflet.polygon([mapPoint1, mapPoint2, mapPoint3], {
      fillColor: "#FF0000",
      fillOpacity: 0.5,
      stroke: false
    });

    this._polygon.addTo(map);
  }
  public onUpdate(
    map: Leaflet.Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void {
    // Nothing
  }
  public onLeaveScene(
    map: Leaflet.Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void {
    if (this._polygon) {
      this._polygon.remove();
    }
  }

  public getType(): string {
    return KillEvent.TYPE;
  }

  public getTypeID(): number {
    return 5;
  }

  public isActiveAt(time: number): boolean {
    return time >= this.Timestamp && time < this.Timestamp + 3;
  }

  public get VictimID(): number {
    return this._victimID;
  }

  public get VictimPosition(): Vector3 {
    return this._victimPosition;
  }

  public get MyPosition(): Vector3 {
    return this._myPosition;
  }
}
