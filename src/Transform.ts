import { Vector3 } from "./Vector3";
import { BufferProxy } from "./BufferProxy";

/**
 * Represents a transformation consisting of a translation and a rotation.
 */
export class Transform {
  public static fromBuffer(buffer: BufferProxy) {
    return new Transform(
      Vector3.fromBuffer(buffer),
      Vector3.fromAngle(buffer.nextFloat32() * (Math.PI / 180))
    );
  }

  public static lerp(from: Transform, to: Transform, dt: number): Transform {
    return new Transform(
      Vector3.lerp(from.Position, to.Position, dt),
      Vector3.lerp(from.Direction, to.Direction, dt).normalize()
    );
  }

  private _position: Vector3;
  private _direction: Vector3;

  public constructor(
    position = new Vector3(0, 0, 0),
    direction = Vector3.Up()
  ) {
    this._position = position;
    this._direction = direction;
  }

  /**
   * Returns a new instance of a Transform that is equal
   * to this one.
   */
  public clone(): Transform {
    return new Transform(
      new Vector3(this._position.x, this._position.y, this._position.z),
      new Vector3(this._direction.x, this._direction.y, this._direction.z)
    );
  }

  public get Position(): Vector3 {
    return this._position;
  }

  public get Direction(): Vector3 {
    return this._direction;
  }
}
