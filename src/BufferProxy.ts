export class BufferProxy {
  private view: DataView;
  private currentOffset = 0;
  private utf8Decoder = new TextDecoder("utf-8");

  constructor(buffer: ArrayBuffer) {
    this.view = new DataView(buffer);
  }

  public nextInt8() {
    const byte = this.view.getInt8(this.currentOffset);
    this.currentOffset += 1;
    return byte;
  }

  public nextBoolean() {
    return this.nextInt8() > 0;
  }

  public nextInt16() {
    const short = this.view.getInt16(this.currentOffset, true);
    this.currentOffset += 2;
    return short;
  }

  public nextInt32() {
    const int = this.view.getInt32(this.currentOffset, true);
    this.currentOffset += 4;
    return int;
  }

  public nextFloat32() {
    const float = this.view.getFloat32(this.currentOffset, true);
    this.currentOffset += 4;
    return float;
  }

  public skip(bytes: number) {
    this.currentOffset += bytes;
  }

  public nextBytes(count: number) {
    const buffer = new Array<number>(count);

    for (let i = 0; i < count; i++) {
      buffer[i] = this.nextInt8();
    }

    return buffer;
  }

  /**
   * Returns a string whose bytes start at the current offset,
   * is encoded in UTF-8 and whose size in bytes is the given size.
   *
   * @param byteCount How many bytes the string takes.
   */
  public nextString(byteCount: number) {
    const bytes = this.nextBytes(byteCount);
    const view = new Uint8Array(bytes);

    return this.utf8Decoder.decode(view);
  }

  public seek(offset: number) {
    if (offset < 0) {
      throw new RangeError("Offset < 0");
    }
    if (offset >= this.view.byteLength) {
      throw new RangeError("Offset greater than buffer length");
    }

    this.currentOffset = offset;
  }

  public getCurrentOffset() {
    return this.currentOffset;
  }
}
