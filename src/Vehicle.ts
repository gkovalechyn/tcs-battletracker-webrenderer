import { Event } from "./Events/Event";
import { Timeline } from "./Timeline";
import { Transform } from "./Transform";
import { VehicleType } from "./VehicleType";
import { Side } from "./Side";
import { BufferProxy } from "./BufferProxy";
import { Snapshot } from "./Snapshot";
import { IDrawable } from "./Drawable";
import * as Leaflet from "leaflet";
import { RasterCoords } from "./RasterCoords";
import "leaflet-rotatedmarker";
import { IconCache } from "./IconCache";
import { VehicleStatus } from "./VehicleStatus";
/**
 * Represents an Arma 3 vehicle being it a unit or an actual vehicle like a car or tank.
 */
export class Vehicle implements IDrawable {
  public static fromBuffer(buffer: BufferProxy): Vehicle {
    const vehicle = new Vehicle(
      buffer.nextInt32(),
      buffer.nextInt32() as VehicleType,
      buffer.nextString(buffer.nextInt32()),
      buffer.nextInt32() as Side,
      buffer.nextBoolean()
    );

    vehicle._createdAt = buffer.nextFloat32();
    vehicle._deletedAt = buffer.nextFloat32();

    vehicle._timeline = Timeline.fromBuffer(buffer);
    return vehicle;
  }

  private _id: number = 0;
  private _type: VehicleType = VehicleType.MAN;
  private _name: string = "";
  private _side: Side = Side.WEST;
  private _timeline: Timeline = new Timeline();
  private _isPlayer: boolean = false;
  private _createdAt: number = 0;
  private _deletedAt: number = 0;

  private _marker: Leaflet.Marker | null = null;
  private _lastSnapshot: Snapshot | null = null;
  private _lastMapUpdateZoom = 0;

  constructor(
    id: number,
    type: VehicleType,
    name: string,
    side: Side,
    isPlayer = false,
    timeline = new Timeline()
  ) {
    this._id = id;
    this._type = type;
    this._name = name;
    this._side = side;
    this._isPlayer = isPlayer;
    this._timeline = timeline;
  }
  /**
   * Checks if this vehicle existed in-game at the given time.
   * The time is in game seconds.
   *
   * @param timestamp The in-game time
   */
  public existsInGameAt(timestamp: number): boolean {
    // Is between the first and last events which are created and deleted.
    return timestamp > this._createdAt && timestamp < this._deletedAt;
  }

  public onEnterScene(
    map: Leaflet.Map,
    time: number,
    coordenatesTransformer: RasterCoords
  ): void {
    const icon = IconCache.getIconFor(
      this.Type,
      VehicleStatus.DEFAULT,
      this._side,
      0
    );
    if (icon) {
      this._marker = Leaflet.marker([0, 0], {
        icon
      });
    } else {
      this._marker = Leaflet.marker([0, 0]);
    }

    this._marker.bindTooltip(this._name).addTo(map);
    this._marker.setRotationOrigin("center center");
    this.updateMarker(map, time, coordenatesTransformer);
  }

  public onUpdate(
    map: Leaflet.Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void {
    this.updateMarker(map, time, coordenatesTransformer);
  }
  public onLeaveScene(
    map: Leaflet.Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void {
    this._marker!.remove();
  }

  /**
   * Gets a snapshot of the vehicle at the given time
   * @param time The in-game time.
   */
  public getSnapshotAt(time: number): Snapshot {
    let low = 0;
    let high = this.Timeline.Snapshots.length - 1;
    const first = this._timeline.Snapshots[low];
    const last = this._timeline.Snapshots[high];

    if (time <= first.Timestamp) {
      return first.clone();
    } else if (time >= last.Timestamp) {
      return last.clone();
    }

    while (low <= high) {
      const middle = Math.floor((low + high) / 2);
      const lhs = this.Timeline.Snapshots[middle];
      const rhs = this.Timeline.Snapshots[middle + 1];

      if (time >= lhs.Timestamp && time <= rhs.Timestamp) {
        return new Snapshot(
          time,
          lhs.Status,
          Transform.lerp(
            lhs.Transform,
            rhs.Transform,
            (time - lhs.Timestamp) / (rhs.Timestamp - lhs.Timestamp)
          )
        );
      } else if (time < lhs.Timestamp) {
        high = middle - 1;
      } else if (time > rhs.Timestamp) {
        low = middle + 1;
      }
    }

    // It is impossible for it to reach here unless there is an error in the algorithm above
    throw new Error("Invalid time parameter");
  }

  /**
   * Returns the active events at the given game time.
   * @param time The game time in seconds
   */
  public getActiveEventsAt(time: number): Event[] {
    const events = this.Timeline.Events;

    if (events.length === 0) {
      return [];
    }

    if (
      time < events[0].Timestamp ||
      time > events[events.length - 1].Timestamp
    ) {
      return [];
    }

    const activeEvents = [] as Event[];
    let low = 0;
    let high = events.length - 1;

    while (low <= high) {
      const middle = Math.floor((low + high) / 2);
      const middleEvent = events[middle];

      if (middleEvent.isActiveAt(time)) {
        for (let i = middle - 1; i >= 0; i--) {
          if (events[i].isActiveAt(time)) {
            activeEvents.push(events[i]);
          } else {
            break;
          }
        }

        activeEvents.push(events[middle]);

        for (let i = middle + 1; i < events.length; i++) {
          if (events[i].isActiveAt(time)) {
            activeEvents.push(events[i]);
          } else {
            break;
          }
        }

        return activeEvents;
      } else if (time > middleEvent.Timestamp) {
        low = middle + 1;
      } else if (time < middleEvent.Timestamp) {
        high = middle - 1;
      }
    }

    return activeEvents;
  }

  public get ID(): number {
    return this._id;
  }

  public get Type(): VehicleType {
    return this._type;
  }

  public get Name(): string {
    return this._name;
  }

  public get Side(): Side {
    return this._side;
  }

  public get Timeline(): Timeline {
    return this._timeline;
  }

  public get IsPlayer(): boolean {
    return this._isPlayer;
  }

  public get CreatedAt(): number {
    return this._createdAt;
  }

  public get DeletedAt(): number {
    return this._deletedAt;
  }

  private updateMarker(
    map: Leaflet.Map,
    time: number,
    transformer: RasterCoords
  ) {
    if (!this._marker) {
      return;
    }

    const snapshot = this.getSnapshotAt(time);

    const transform = snapshot.Transform;
    const leafletPosition = transformer.toLeaflet([
      transform.Position.x,
      transformer.getHeight() - transform.Position.y
    ]);

    // Only update if the current position is visible or the last one is
    // Because while we may not be looking at where the marker is now
    // we may be looking at where the not-updated one is, and we need
    // to make sure it's not visible
    if (!map.getBounds().contains(leafletPosition)) {
      // If the old position is visible, we need to update it to
      // put it in it's proper place
      if (this._lastSnapshot != null) {
        const lastPosition = this._lastSnapshot.Transform.Position;

        const lastLeafletPosition = transformer.toLeaflet([
          lastPosition.x,
          transformer.getHeight() - lastPosition.y
        ]);

        if (!map.getBounds().contains(lastLeafletPosition)) {
          return;
        }
      }
    }

    // Dont update the marker if it doesn't need to be
    // Arbitrary values, need more testing
    if (this._lastSnapshot) {
      const lastTransform = this._lastSnapshot.Transform;
      if (
        this._lastMapUpdateZoom === map.getZoom() &&
        lastTransform.Direction.subtracted(
          snapshot.Transform.Direction
        ).squaredMagnigude() < 0.5 &&
        lastTransform.Position.subtracted(
          snapshot.Transform.Position
        ).squaredMagnigude() < 0.5 &&
        this._lastSnapshot.Status === snapshot.Status
      ) {
        return;
      }
    }

    this._lastSnapshot = snapshot;
    this._lastMapUpdateZoom = map.getZoom();

    const icon = IconCache.getIconFor(
      this.Type,
      snapshot.Status,
      this._side,
      map.getZoom()
    );

    if (icon) {
      this._marker.setIcon(icon);
    }

    this._marker.setLatLng(leafletPosition);
    this._marker.setRotationAngle(
      (transform.Direction.getAngleXAxis() * 180) / Math.PI
    );
  }
}
