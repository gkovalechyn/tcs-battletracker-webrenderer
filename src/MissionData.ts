import { Vector3 } from "./Vector3";
import { BufferProxy } from "./BufferProxy";
/**
 * Contains basic mission data like what world the mission was conducted,
 * what is the mission name, the world size and when it started and ended.
 */
export class MissionData {

  public static fromBuffer(buffer: BufferProxy) {
    return new MissionData(
      buffer.nextString(buffer.nextInt32()),
      buffer.nextString(buffer.nextInt32()),
      Vector3.fromBuffer(buffer),
      buffer.nextFloat32(),
      buffer.nextFloat32()
    );
  }

  private _world: string;
  private _mission: string;
  private _worldSize: Vector3;
  private _startTime: number;
  private _endTime: number;

  constructor(
    world: string,
    mission: string,
    worldSize: Vector3,
    startTime: number,
    endTime: number
  ) {
    this._world = world;
    this._mission = mission;
    this._worldSize = worldSize;
    this._startTime = startTime;
    this._endTime = endTime;
  }

  public get WorldName(): string {
    return this._world;
  }

  public set WorldName(v: string) {
    this._world = v;
  }

  public get MissionName(): string {
    return this._mission;
  }

  public set MissionName(v: string) {
    this._mission = v;
  }

  public get WorldSize(): Vector3 {
    return this._worldSize;
  }

  public set WorldSize(v: Vector3) {
    this._worldSize = v;
  }

  public get StartTime(): number {
    return this._startTime;
  }

  public set StartTime(v: number) {
    this._startTime = v;
  }

  public get EndTime(): number {
    return this._endTime;
  }

  public set EndTime(v: number) {
    this._endTime = v;
  }
}
