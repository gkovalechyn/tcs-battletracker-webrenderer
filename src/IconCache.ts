import * as Leaflet from "leaflet";
import { VehicleType } from "./VehicleType";
import { VehicleStatus } from "./VehicleStatus";
import { Side } from "./Side";

// Side -> Type -> Status -> Zoom level -> Icon
interface IconMap {
  // Side -> Type
  [side: number]: {
    // Type -> Status
    [type: number]: {
      // Status -> zoom level icons.
      [status: number]: {
        // Zoom level -> Icon
        [zoomLevel: number]: Leaflet.Icon;
      };
    };
  };
}

class IconCacheClass {
  /**
   * For which zoom levels the icons will be created
   * index is the zoom level, value is the icon size
   */
  private _iconSizes = [50, 45, 40, 35, 30, 25, 20];

  private icons: IconMap = {};

  public createIconsFor(
    url: string,
    type: VehicleType,
    status: VehicleStatus,
    side: Side
  ) {
    if (!this.icons[side]) {
      this.icons[side] = {};
    }

    const sideIcons = this.icons[side];
    if (!sideIcons[type]) {
      sideIcons[type] = {};
    }

    const typeIcons = sideIcons[type];
    if (!typeIcons[status]) {
      typeIcons[status] = {};
    }

    const statusIcons = typeIcons[status];
    let zoomLevel = 0;
    for (const size of this._iconSizes) {
      if (size > 0) {
        const icon = Leaflet.icon({
          iconUrl: url,
          iconSize: [size, size],
          iconAnchor: [size / 2, size / 2]
        });

        statusIcons[zoomLevel] = icon;
      }

      zoomLevel++;
    }
  }

  public getIconFor(
    type: VehicleType,
    status: VehicleStatus,
    side: Side,
    zoomLevel: number
  ): Leaflet.Icon | null {
    const sideIcons = this.icons[side];
    if (!sideIcons) {
      return null;
    }

    const typeIcons = sideIcons[type];
    if (!typeIcons) {
      return null;
    }

    const statusIcons = typeIcons[status];
    if (!statusIcons) {
      return null;
    }

    if (statusIcons[zoomLevel]) {
      return statusIcons[zoomLevel];
    }

    // TODO load the nearest icon of to that zoom level
    return statusIcons[zoomLevel];
  }

  public getIconSizeAtZoomLevel(zoomLevel: number) {
    if (zoomLevel >= this._iconSizes.length) {
      return this._iconSizes[this._iconSizes.length - 1];
    }

    if (zoomLevel < 0) {
      return this._iconSizes[0];
    }

    if (this._iconSizes[zoomLevel] <= 0) {
      for (
        let previousZoom = zoomLevel - 1;
        previousZoom >= 0;
        previousZoom--
      ) {
        if (this._iconSizes[previousZoom] >= 0) {
          return this._iconSizes[previousZoom];
        }
      }

      return this._iconSizes[0];
    }

    return this._iconSizes[zoomLevel];
  }

  /**
   * Sets the icon sizes for the given zoom levels.
   * Does not affect already created icons, only affects the creation of new ones.
   *
   * @param zoomLevels The zoom levels
   */
  public setIconSizes(iconSizes: number[]) {
    this._iconSizes = iconSizes;
  }

  /**
   * Gets the current icon sizes for the zoom levels.
   */
  public getIconSizes() {
    return this._iconSizes;
  }
}

export const IconCache = new IconCacheClass();
