import * as Leaflet from "leaflet";
import { IDrawable } from "@/Drawable";
import { RasterCoords } from "@/RasterCoords";

export abstract class Event implements IDrawable {
  private _timestamp: number;

  public constructor(timestamp: number) {
    this._timestamp = timestamp;
  }

  /**
   * Returns what type of event this one is.
   */
  public abstract getTypeID(): number;

  /**
   * Checks if the this event is active at the given time.
   * By active mening it still has an effect on the unit that this event belongs to.
   *
   * @param time The timestamp
   */
  public abstract isActiveAt(time: number): boolean;

  /**
   * Returns when the event happened.
   * The value is the amount of seconds since the start of the mission.
   */
  public get Timestamp(): number {
    return this._timestamp;
  }

  public abstract onEnterScene(
    map: Leaflet.Map,
    time: number,
    coordenatesTransformer: RasterCoords
  ): void;
  public abstract onUpdate(
    map: Leaflet.Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void;
  public abstract onLeaveScene(
    map: Leaflet.Map,
    coordenatesTransformer: RasterCoords,
    time: number
  ): void;
}
