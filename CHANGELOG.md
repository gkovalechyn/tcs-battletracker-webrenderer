# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 1.0.1 - 20/07/2019
### Fixed
* Units that were not visible were not being updated, but when looking at their old markers they weren't being put in the right place.

## 1.0.0 - 07/07/2019
### Changed
* Map drawing is now handled by leaflet and now requires a tiling program to draw the maps correctly.

### Removed
* Support for the JSON file format, now only the binary file format is accepted.
* BattleTracker play and resume deprecated functions.

## 0.3.4 - 2019-07-03
### Added
* Support for also drawing HTMLImageElements to prevent so much memory usage.

## 0.3.3 - 2019-07-01
### Fixed
* Fixed Hit and Killed event comments regarding scale.

## 0.3.2 - 2019-07-01
### Fixed
* Documentation of the recording version getter.
* Documentation of the magic values.

### Changed
* Simplified Hit and Kill event drawing.
* Hit and Kill events are drawn with an "arrow" from the instigator to the target.
* Changed Vector3::subtracted parameter name.

## 0.3.1 - 2019-06-28
### Fixed
* Events not being rendered to their end due to how they were checked if they were still "active".
* Documentation for a few classes.

### Deprecated
* Recording::fromJson. All recordings now should be in the binary format.

## 0.3.0 - 2019-03-25
### Changed
* The map is now rendered even if the recording is not being played.
* You can now zoom while the recording is not being played.
* If no recording is provided it uses the map image size as world size.

## 0.2.6 - 2019-03-24
### Fixed
* pause() had the same effect as stop();

## 0.2.5 - 2019-03-24 
### Added
* Customizable scaling factor

# 0.2.4 - 2019-03-24
### Fixed
* Being able to make the scale smaller than 0;
* Wrong line in the readme.
* It doesn't stop playing the recording after it reaches the end of the recording.

### Added
* Option to toggle the debug information display.

### Changed
* Stop function now returns the recording to the start.
* When loading a recording the current recording time is now the start of the mission.

### Deprecated
* start() and resume() functions will be replaced by play();

## 0.2.3 - 2019-03-21
### Added
* X and Y lines in case there is no map image.

### Changed
* Scaling now moves the camera where it should be so it's viewport doesn't jump around when scaling.

## 0.2.2 - 2019-03-19
### Fixed
* CurrentRecordingTime was returning the getter itself and not the actual value.

## 0.2.1 - 2019-03-19
### Added
* isPlaying() method.

## 0.2.0 - 2019-03-19
### Added
*  Support for rendering the new binary format.

## 0.1.0 - 2019-03-07
### Added
* Support for rendering recording format v0.1.0
