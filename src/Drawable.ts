import * as Leaflet from "leaflet";
import { RasterCoords } from "./RasterCoords";

export interface IDrawable {
  onEnterScene(map: Leaflet.Map, time: number, coordenatesTransformer: RasterCoords): void;
  onUpdate(map: Leaflet.Map, coordenatesTransformer: RasterCoords, time: number): void;
  onLeaveScene(map: Leaflet.Map, coordenatesTransformer: RasterCoords, time: number): void;
}
